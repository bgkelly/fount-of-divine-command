(function(){
    var randomElement = function(arr){
        var idx = Math.floor(Math.random() * 100) % arr.length;

        return arr[idx];
    };

    var replaceKeyword = function(scaffold, key) {
        var placeholder = "{" + key + "}",
            containsPlaceholder = scaffold.includes(placeholder);

        while(containsPlaceholder) {
            let part = randomElement(partsDict[key]);

            scaffold = scaffold.replace(placeholder, part);

            containsPlaceholder = scaffold.includes(placeholder);
        }

        return scaffold;
    };

    //replaces certain keys with another at random
    var prepReplaceableKeys = function(scaffold){
        var replaceableKeys = {
            "call_to_action": "{direct_action} {direct_object}"
        };

        Object.keys(replaceableKeys).forEach(key => {
            var placeholder = "{" + key + "}",
                containsPlaceholder = scaffold.includes(placeholder),
                keyCount = scaffold.split(placeholder).length - 1,
                currentCount = 0;

            while(containsPlaceholder && currentCount < keyCount) {
                currentCount++;

                //50/50 chance to replace the key
                var shouldReplace = (Math.floor(Math.random() * 100) % 2) == 0;

                if(!shouldReplace){
                    continue;
                }

                scaffold = scaffold.replace(placeholder, replaceableKeys[key]);
                containsPlaceholder = scaffold.includes(placeholder);
            }
        });

        return scaffold;
    };

    var generatePhrase = function(){
        var scaffold = randomElement(scaffolds);

        scaffold = prepReplaceableKeys(scaffold);

        //go through each key and replace them if they appear in the scaffold
        Object.keys(partsDict).forEach((key) => {
            scaffold = replaceKeyword(scaffold, key);
        });

        return scaffold;
    };

    var scaffolds = [
        "{divine_call}, {call_to_action} and {call_to_action} of {diety}",
        "{divine_call}, {call_to_action}, {call_to_action}",
        "With mine {direct_object}, I {call_to_action}",
        "{divine_call}, {call_to_action}",
        "{divine_call}, {call_to_action}, and {call_to_action}!",
        "{divine_call} of {diety}, {call_to_action}! {call_to_action}!",
        "{divine_call}, {call_to_action}, so that I may {call_to_action}!",
        "{divine_call}, I am {direct_object}! {call_to_action} that I may {call_to_action}!",
        "{divine_call}, {call_to_action}! {call_to_action}",
        "{call_to_action}, {divine_call}! {call_to_action}!",
        "{direct_action} {direct_object} before {direct_object}, {divine_call}",
        "{divine_call}, may you {call_to_action} with {direct_object}!",
        "I call upon thee, {divine_call}, {divine_call}, {call_to_action}. {call_to_action}!",
        "{call_to_action}, {divine_call}! {call_to_action}"
    ];

    var partsDict = {
        "divine_call": [
            "masters of the sublunary sphere",
            "oh lord of divine hatred",
            "dark gods of the furthest sphere",
            "Oh enkindler of the flame of justice",
            "Shepherds of the lost souls",
            "Lord of unclaimed names",
            "Earth, Wind, Fire, Water",
            "Immortal Arbiter",
            "Rock of Ages",
            "Absolute Administrator",
            "Blessed are you, oh lord our God, king of the universe",
            "Oh Adonai Eloheinu",
            "Holder of the unspeakable power",
            "Lord of Infinite Faces",
            "Arbiter of Arbiters",
            "Great be Yaweh, your name exalted",
            "absolute arbitrator",
            "mother of mothers",
            "oh court of heaven"
        ],
        "call_to_action": [
            "embrace mine soul gently in thine merciful hands",
            "grant me thine power",
            "I beseech thee",
            "send thee to a watery grave",
            "let me taste of the honey of victory",
            "flood the world with thine madness",
            "cast thine judgements upon the whelps before me",
            "Fill me with your burning passion",
            "execute the glorious rule of Greater Heaven",
            "let the dams of your wrath burst",
            "let thine name burn upon the lips of the unclean",
            "Cast those sin-laden before me unto the sea of the damned, and let them sink as stones",
            "Grant me your salvation and your defense",
            "Let me be thine sieve"
        ],
        "direct_action": [
            "whisper into mine ears",
            "let your hunger destroy",
            "do honor unto the name of",
            "unleash thine righteous beasts that they may rip",
            "let the unknowable chaos of thine mind rip asunder",
            "nourish the seeds of vengeance upon",
            "let surge the rivers with",
            "Let me be the sword which administers to",
            "let thine sheltering tower become",
            "shape this world of sin into",
            "split asunder",
            "Drown this world in the oceans of",
            "set their souls alight with",
            "Give unto me the hammer of",
            "Let tremble even",
            "burn nobly",
            "draw out as poison from a wound",
            "Guide my hand that it may smite",
            "Bestow upon my outstretched arms"
        ],
        "direct_object": [
            "powerful words",
            "the shattered soul of mine enemy",
            "the brilliant ones",
            "the flesh of the unclean",
            "the firmament of our reality",
            "the firmament",
            "the tears of the angels",
            "your infinite justice",
            "the lightning rod of judgement",
            "a mirror of the kingdom of heaven",
            "your vessel",
            "The unholy vessels of the serpent",
            "might",
            "your unknowable glory",
            "primal terror",
            "your mighty walls",
            "the mightly lords of Canaan",
            "the servents of the serpent",
            "thine pure fingers of light",
            "the corruption from this earth",
            "the lance of longinus",
            "the abominations of the wicked demiurge"
        ],
        "diety": [
            "Melchior",
            "Higan",
            "Maccabee"
        ]
    };
    
    document.querySelector("button").addEventListener("click", (e) => {
        e.preventDefault();
        
        var scaffold = generatePhrase();

        document.querySelector("#output").innerHTML = scaffold;

        return false;
    });
}());